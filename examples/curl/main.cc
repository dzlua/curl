#include <curl.h>
#include <string>
#include <iostream>

#include "api.h"

static size_t writeFile(void *contents, size_t size, size_t num, void *udata) {
  auto msg = (std::string*)udata;
	size_t realSize = size * num;

	if (realSize && msg) {
    msg->append((const char*)contents, realSize);
	}

	return realSize;
}

int main(int argc, char *argv[]) {
  std::string msg = "";

  curl_global_init(CURL_GLOBAL_ALL);

  CURLcode err = CURLcode::CURL_LAST;
  auto curl = curl_easy_init();
  if (curl) {
    curl_easy_setopt(curl, CURLoption::CURLOPT_URL, api::lyric);
    curl_easy_setopt(curl, CURLoption::CURLOPT_WRITEFUNCTION, writeFile);
    curl_easy_setopt(curl, CURLoption::CURLOPT_WRITEDATA, &msg);
    err = curl_easy_perform(curl);
  }
  curl_easy_cleanup(curl);

  curl_global_cleanup();

  std::cout << msg << std::endl;
  api::write2file("curl.lrc", msg);
  std::cout << "----------" << std::endl;
  std::cout << "ERROR:" << curl_easy_strerror(err) << std::endl;
  std::cout << "CODE:" << (int)err << std::endl;
  return 0;
}