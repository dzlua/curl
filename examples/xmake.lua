local function addTarget(t)
  target(t)
    set_targetdir("../bin")
    add_includedirs("..", ".")
    add_includedirs("../pkg/curl.pkg/inc")
    add_packages("curl")
    set_kind("binary")
    add_files(t .. "/**.cc")
    if is_os("windows") then
      add_links("WS2_32")
    end
  target_end()
end

addTarget("curl")
addTarget("easy")
addTarget("multi")
addTarget("getRespHeader")