#include <curl.hpp>
#include <string>
#include <iostream>

#include "api.h"

static size_t writeFile(void *contents, size_t size, size_t num, void *udata) {
  auto msg = (std::string*)udata;
	size_t realSize = size * num;

	if (realSize && msg) {
    msg->append((const char*)contents, realSize);
	}

	return realSize;
}

int main(int argc, char *argv[]) {
  std::string msg = "";

  curl::globalInit(CURL_GLOBAL_ALL);

  CURLcode err = CURLcode::CURL_LAST;
  auto easy = curl::easy();
  if (easy) {
    easy->setopt(CURLoption::CURLOPT_URL, api::lyric);
    easy->setopt(CURLoption::CURLOPT_WRITEFUNCTION, writeFile);
    easy->setopt(CURLoption::CURLOPT_WRITEDATA, &msg);
    err = easy->perform();
    easy->cleanup();
  }
  
  curl::globalCleanup();

  std::cout << msg << std::endl;
  api::write2file("easy.lrc", msg);
  std::cout << "----------" << std::endl;
  std::cout << "ERROR:" << curl::strError(err) << std::endl;
  std::cout << "CODE:" << (int)err << std::endl;

  return 0;
}