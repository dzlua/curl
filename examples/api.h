#ifndef _API_H_
#define _API_H_

#include <fstream>
#include <string>

namespace api {
  static const char* lyric = "https://music.163.com/api/song/media?id=1456200611&format=json";
  static const char* music = "http://m10.music.126.net/20200930113713/8dee5b459f6cdd372ead71d423d7d2ea/ymusic/obj/w5zDlMODwrDDiGjCn8Ky/2909791690/453d/51f0/2c7c/492317cb130897ab52acff4d9de83c58.mp3";

  void write2file(const char *file, void *content, size_t size) {
    std::ofstream of(file, std::ios::binary);
    if (of.is_open()) {
      of.write((const char*)content, size);
      of.close();
    }
  }
  void write2file(const char *file, const std::string &content) {
    write2file(file, (void*)content.data(), content.size());
  }
} // end namespace api

#endif // _API_H_