set_project("curl")
set_version("1.0.0")
set_languages("c99", "cxx11")
add_rules("mode.debug", "mode.release")

add_packagedirs("pkg")
includes("examples/xmake.lua")