-- v7.72.0
option("curl")
set_showmenu(true)
set_description("The curl package")
add_links("curl")
add_linkdirs("lib/$(plat)/$(arch)")
add_includedirs("inc/$(plat)", "inc")